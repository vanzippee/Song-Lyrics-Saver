import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:silver_river_lyrics/model/errorMessage.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_database/firebase_database.dart';
import 'package:silver_river_lyrics/model/songList.dart';
import 'package:silver_river_lyrics/model/song.dart';

class ListLyrics extends StatefulWidget {
  @override
  _ListLyricsState createState() => new _ListLyricsState();
}

class _ListLyricsState extends State<ListLyrics> {
  List values;

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  StreamSubscription<Event> _onSongAddedSubscription;
  Query _songQuery;

  @override
  void initState() {
    super.initState();

    _songQuery = _database.reference().child("songs");
    _onSongAddedSubscription = _songQuery.onChildAdded.listen(onEntryAdded);
  }

  @override
  void dispose() {
    _onSongAddedSubscription.cancel();
    super.dispose();
  }

  onEntryAdded(Event event) {
    setState(() {
      List songListItem = [
        Uri.decodeFull(event.snapshot.value['title']),
        Uri.decodeFull(event.snapshot.value['artist']),
        Uri.decodeFull(event.snapshot.value['uname'])
      ];
      values.add(songListItem);
    });
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = FutureBuilder(
      future: _getData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          default:
            if (snapshot.hasError)
              return Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot);
        }
      },
    );

    return Scaffold(
      body: Center(child: futureBuilder),
    );
  }

  Future<List> _getData() async {
    String url =
        'https://song-lyrics-saver.nn.r.appspot.com/lyrics/api/v0.1/songlist';
    final response =
        await http.get(url, headers: {"Accept": "application/json"});
    SongList songList = SongList.fromJson(json.decode(response.body));
    values = songList.songs;
    return values;
  }

  void _getSong(requestUrl) async {
    final response = await http.get(requestUrl);
    print("Response Code: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      Song savedSong = Song.fromJson(json.decode(response.body));
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ShowSong(savedSong)));
    } else {
      displayAlert(context);
    }
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<dynamic> values = snapshot.data;
    return new ListView.builder(
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          elevation: 2.0,
          child: ListTile(
            title: Text(Uri.decodeFull(values[index][0])),
            subtitle: Text(Uri.decodeFull(values[index][1])),
            trailing: CircleAvatar(
              child: Text(
                values[index][2].length > 5
                    ? '${values[index][2].substring(0, 3)}...'
                    : values[index][2],
                textScaleFactor: 0.5,
              ),
            ),
            onTap: () {
              String requestUrl =
                  "https://song-lyrics-saver.nn.r.appspot.com/lyrics/api/v0.1/song-lyrics/" +
                      (values[index][1] + ": " + values[index][0])
                          .replaceAll(".", "");
              debugPrint(requestUrl);
              _getSong(requestUrl);
            },
          ),
        );
      },
    );
  }
}

class ShowSong extends StatefulWidget {
  final Song savedSong;

  ShowSong(this.savedSong);

  @override
  State<StatefulWidget> createState() => _ShowSong();
}

class _ShowSong extends State<ShowSong> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Uri.decodeFull(widget.savedSong.title)),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Text(Uri.decodeFull(widget.savedSong.key)),
            Text(Uri.decodeFull(widget.savedSong.uname)),
            Expanded(
                flex: 1,
                child: SingleChildScrollView(
                  child: Text(Uri.decodeFull(widget.savedSong.lyrics)),
                )),
          ],
        ),
      ),
    );
  }
}
