Frontend: Flutter mobile phone frontend to search for and save song lyrics
	- Main Screens: data entry and view saved results
	- Navigation: TabBar

	- Data entry:
		- text entry for artist name and song title
		- search button active only when both fields are full
		- button performs seach using GET request to http://api.chartlyrics.com/apiv1.asmx/SearchLyricDirect?artist=string&song=string
		- displays "Lyrics not found" alert if the request returns no lyrics
			(500: body: SearchLyricDirect: No valid words left in contains list, this could be caused by stop words.)
		- displays a new screen with the song title, and lyrics if the request returns lyrics with save button
			(200: body: <Lyric>"Here the lyrics of the song"</Lyric> XML object)
		- pushing save button displays popup with box for username and save button to send 
			username, artist, song title, and lyrics to backend to save in firebase db.

	- Display results:
		- displays scrollable list of saved songs
			Artist, Song Title, username of first user to save song.
		- list auto updates with change to database
		- tapping on an item resulst in a screen that displays
			the artist name, the song title, and the lyrics
        - Google Real Time database updates the song list in real time
Backend: Python Google App Engine REST backend
	- saves information from app to database
	- retrieves information from the database 
	    - stream monitors database for changes and updates local dictionary when songs are added.
	- api sends either a list of songs (200: body: {'list': [<list items [song title, song artist, username]>]})
	    or the information for a single song (200: body: {'key': <>, 'uname': <>, 'artist': <>, 'title': <>, 'lyrics': <>, 'img_url' })
	- responses are URI encoded
