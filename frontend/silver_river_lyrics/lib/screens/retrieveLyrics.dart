import 'dart:async';
import 'package:flutter/material.dart';
import 'package:silver_river_lyrics/model/song.dart';
import 'package:silver_river_lyrics/model/errorMessage.dart';
import 'package:http/http.dart' as http;
import 'package:xml/xml.dart' as xml;

Future<List> fetchAlbum(requestUrl) async {
  String checkUrl =
      "https://song-lyrics-saver.nn.r.appspot.com/lyrics/api/v0.1/song-lyrics/" +
          (Uri.encodeFull(requestUrl[0]) + ": " + Uri.encodeFull(requestUrl[1]))
              .replaceAll(".", "");
  String getUrl =
      "http://api.chartlyrics.com/apiv1.asmx/SearchLyricDirect?artist=" +
          (Uri.encodeFull(requestUrl[0] + "&song=" + requestUrl[1]));
  var r1 = http.get(checkUrl);
  var r2 = http.get(getUrl);
  var response = await Future.wait([r1, r2]); // list of Responses

  if (response[0].statusCode == 200) {
    return [306, null];
  } else if (response[1].statusCode == 200) {
    final xmlDoc = xml.parse(response[1].body);
    try {
      final song = Song.fromObject({
        'artist': xmlDoc
            .findAllElements('LyricArtist')
            .map((node) => node.text)
            .first,
        'title':
            xmlDoc.findAllElements('LyricSong').map((node) => node.text).first,
        'lyrics':
            xmlDoc.findAllElements('Lyric').map((node) => node.text).first,
        'imgUrl': xmlDoc
            .findAllElements('LyricCovertArtUrl')
            .map((node) => node.text)
            .first,
      });
      return [response[1].statusCode, song];
    } catch (_) {
      return [500, null];
    }
  } else {
    return [response[0].statusCode, null];
  }
}

class RetrieveLyrics extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RetrieveLyricsState();
}

class _RetrieveLyricsState extends State<RetrieveLyrics> {
  TextEditingController artistController = TextEditingController();
  TextEditingController songController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _buttonActive = false;

  Future<void> _getLyrics() async {
    String _artistRequest = artistController.text;
    String _songRequest = songController.text;
    List _requestDetails = [_artistRequest, _songRequest];

    if (_buttonActive) {
      _buttonActive = false;
      List resp = await fetchAlbum(_requestDetails);
      if (resp[0] == 200) {
        Song songInfo = resp[1];
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SaveLyrics(songInfo)));
      } else if (resp[0] == 306) {
        duplicateAlert(context);
      } else {
        displayAlert(context);
      }
    }
  }

  //String _getSongInfo()
  void _activateButton() {
    if (artistController.text.length > 0 && songController.text.length > 0) {
      setState(() {
        _buttonActive = true;
      });
    } else {
      setState(() {
        _buttonActive = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/silver-stream-bg.jpg"),
                fit: BoxFit.fitHeight)),
        child: Form(
          key: _formKey,
          autovalidate: _buttonActive,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Silver River Lyrics',
                  style: TextStyle(
                    fontFamily: "Monoton",
                    fontSize: 40.0),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Input information to retrieve song lyrics:',
                  style: TextStyle(
                    fontFamily: "CabinSketch",
                    fontSize: 25.0),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                    controller: artistController,
                    decoration: InputDecoration(
                        hintText: "e.g. Pink Floyd",
                        labelText: "Artist/Band Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                    onChanged: (String value) {
                      _activateButton();
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                    controller: songController,
                    decoration: InputDecoration(
                        hintText: "e.g. Another Brick in the Wall",
                        labelText: "Song Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                    onChanged: (String value) {
                      _activateButton();
                    }),
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                        color: _buttonActive
                            ? Theme.of(context).primaryColorDark
                            : Theme.of(context).primaryColorLight,
                        textColor: Theme.of(context).primaryColorLight,
                        onPressed: () {
                          _getLyrics();
                          _activateButton();
                        },
                        child: Text(
                          'Get Lyrics',
                          textScaleFactor: 1.5,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                        color: Theme.of(context).primaryColorDark,
                        textColor: Theme.of(context).primaryColorLight,
                        onPressed: () {
                          artistController.clear();
                          songController.clear();
                          _activateButton();
                        },
                        child: Text(
                          'Reset',
                          textScaleFactor: 1.5,
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SaveLyrics extends StatefulWidget {
  final Song song;

  SaveLyrics(this.song);

  @override
  State<StatefulWidget> createState() => _SaveLyricsState();
}

class _SaveLyricsState extends State<SaveLyrics> {
  TextEditingController unameController = TextEditingController();
  bool _saveActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.song.title),
      ),
      body: Container(
          /*decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(widget.song.imgUrl)
          )
        ),*/
          margin: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Text(widget.song.key),
              TextField(
                  controller: unameController,
                  decoration: InputDecoration(
                      hintText: "e.g. user",
                      labelText: "Your Username",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                  onChanged: (String value) {
                    if (unameController.text.length > 0) {
                      setState(() {
                        _saveActive = true;
                      });
                    } else {
                      setState(() {
                        _saveActive = false;
                      });
                    }
                  }),
              RaisedButton(
                  color: _saveActive
                      ? Theme.of(context).primaryColorDark
                      : Theme.of(context).primaryColorLight,
                  textColor: Theme.of(context).primaryColorLight,
                  onPressed: () {
                    _saveLyrics();
                  },
                  child: Text(
                    'Save Lyrics',
                    textScaleFactor: 1.5,
                  )),
              Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                    child: Text(widget.song.lyrics),
                  ))
            ],
          )),
    );
  }

  _makePutRequest() async {
    String user = unameController.text;
    String url =
        'https://song-lyrics-saver.nn.r.appspot.com/lyrics/api/v0.1/songlist';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"key": "' +
        Uri.encodeFull(widget.song.key) +
        '", "uname": "' +
        Uri.encodeFull(user) +
        '", "artist": "' +
        Uri.encodeFull(widget.song.artist) +
        '", "title": "' +
        Uri.encodeFull(widget.song.title) +
        '", "lyrics": "' +
        Uri.encodeFull(widget.song.lyrics) +
        '", "song_name": "' +
        Uri.encodeFull(widget.song.key) +
        '", "imgUrl": "' +
        Uri.encodeFull(widget.song.imgUrl) +
        '"}';
    print(json);
    http.Response response = await http.post(url, headers: headers, body: json);
    return response;
  }

  Future<void> _saveLyrics() async {
    if (_saveActive) {
      http.Response resp = await _makePutRequest();
      print(resp.statusCode);
      if (resp.statusCode == 201) {
        Navigator.pop(context);
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Api Response'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text("Server Error"),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Back'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }
}
