import 'package:flutter/material.dart';
import 'package:silver_river_lyrics/screens/retrieveLyrics.dart';
import 'package:silver_river_lyrics/screens/viewLyrics.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Color(0xff03031A),
          accentColor: Color(0xff0A0A66),
          fontFamily: "SpecialElite"),
      title: 'Silver River Lyrics',
      home: MyHomePage(title: 'Silver River Lyrics'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.playlist_add)),
                Tab(icon: Icon(Icons.playlist_play)),
              ],
            ),
          ),
          body: TabBarView(children: [
            RetrieveLyrics(),
            ListLyrics(),
          ]),
        ));
  }
}
