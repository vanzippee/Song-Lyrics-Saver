import pyrebase
from flask import Flask, request
from titlecase import titlecase

config = {
    'apiKey': 'apiKey',
    'authDomain': 'song-lyrics-saver.firebaseapp.com',
    'databaseURL': 'https://song-lyrics-saver.firebaseio.com/',
    'storageBucket': 'song-lyrics-saver.appspot.com',
    # 'serviceAccount': '/home/vanzsr/Documents/credentials/song-lyrics-saver-firebase-adminsdk-piro2-57efe633bc.json'
}


firebase = pyrebase.initialize_app(config)
db = firebase.database()

song_data = {}

# Data needs to me initialized when app engine starts. 
# If it isn't immediate requests will return an empty list.  
song_data = db.child("songs").get().val()

app = Flask(__name__)


def stream_handler(message):
    """Takes in the stream message and adds the new entries to 
    the dictionary. 

    Arguments:
        message {stream} -- Database object that contains the "event", 
        "path", and "data". 
        Event   =   type of database event (CRUD)
        Path    =   database location relative to request url
        Data    =   the content of the event 
    """

    print(message['event'])
    
    print(message['data'])
    print(message['path'])
    if message['event'] == 'put':
        for k, v in message['data'].items():
            song_data[k] = v
    elif message['event'] == 'patch':
        song_data[message['path']] = message['data']
    else:
        print('******Stream not handeled.******')


songlist_stream = db.child("songs").stream(stream_handler)


@app.route('/lyrics/api/v0.1/songlist', methods=['GET'])
def get_songlist():
    """API response for songlist from database of saved songs.
    
    Returns:
        dictionary -- Key is list, values are a list with general song title,
                        artist and username from songs dictionary.
    """
    song_list = []
    # all_songs = db.child("songs").get()
    
    for k, v in song_data.items():
        print(v)
        song_list.append([v["title"], v["artist"], v["uname"]])
    return {"list": sorted(song_list, key=lambda title: title[0])}


@app.route('/lyrics/api/v0.1/song-lyrics/<song_name>', methods=['GET'])
def get_lyrics(song_name):
    """API response for song info from database of using specific song name.
    
    Returns:
        dictionary -- Keys are: artist, title, lyrics and username.
    """
    
    song = (db.child("songs").get().val())[titlecase(song_name)]
    return song


@app.route('/lyrics/api/v0.1/songlist', methods=["POST"])
def add_song():
    """POST method for accessing API to write song information
    to the database. 
    
    Returns:
        dictionary  -- information for added song
        status code -- 201 for successful write
    """

    new_song = {
        'key': titlecase(request.json['key']),
        'uname': titlecase(request.json['uname']),
        'artist': titlecase(request.json['artist']),
        'title': titlecase(request.json['title']),
        'lyrics': titlecase(request.json['lyrics']),
        'img_url': titlecase(request.json['imgUrl'])
    }

    db.child("songs").child(request.json['song_name']).update(new_song)
    return new_song, 201


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8080, debug=True)
