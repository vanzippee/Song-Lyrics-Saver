class Song {
  String key;
  String artist;
  String title;
  String lyrics;
  String imgUrl;
  String uname;

  Song(this.key, this.artist, this.title, this.lyrics, this.imgUrl);
  Song.savedSong(
      this.uname, this.key, this.artist, this.title, this.lyrics, this.imgUrl);

  Song.fromObject(dynamic o) {
    this.key = (o['artist'] + ": " + o['title']).replaceAll(".", "");
    this.artist = o['artist'];
    this.title = o['title'];
    this.lyrics = o['lyrics'];
    this.imgUrl = o['imgUrl'];
    try {
      this.uname = o['uname'];
    } catch (_) {
      print("No uname given");
    }
  }

  Song.fromJson(Map<String, dynamic> json) {
    this.key = json['key'].replaceAll(".", "");
    this.artist = json['artist'];
    this.title = json['title'];
    this.lyrics = json['lyrics'];
    this.imgUrl = json['imgUrl'];
    this.uname = json['uname'];
  }
}
