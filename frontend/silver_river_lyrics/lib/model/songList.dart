class SongList {
  List songs;

  List get songsList {return songs;}

  SongList(this.songs);

  SongList.fromJson(Map<String, dynamic> json) {
    this.songs = json["list"];
  }

}
