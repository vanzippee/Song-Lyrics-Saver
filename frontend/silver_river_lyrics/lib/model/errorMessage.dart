import 'package:flutter/material.dart';

displayAlert(context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Api Response'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("No lyrics found"),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Back'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

duplicateAlert(context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Api Response'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("Song Already Saved"),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Back'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
